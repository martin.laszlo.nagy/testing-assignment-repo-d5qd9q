﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    public class SearchPageImdb : BasePage
    {
        public SearchPageImdb(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static SearchPageImdb Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.imdb.com/find?q=&ref_=nv_sr_sm";
            return new SearchPageImdb(webDriver);
        }

        public SearchWidgetImdb GetSearchWidget()
        {
            return new SearchWidgetImdb(Driver);
        }

        public ResultWidgetImdb GetResultWidget()
        {
            return new ResultWidgetImdb(Driver);
        }
    }
}
