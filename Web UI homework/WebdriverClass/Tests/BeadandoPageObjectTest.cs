﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebdriverClass.Models;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.Tests
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("MovieData")]
        public void PageObjectSearch(String searchName, String expectedText)
        {
            SearchWidgetImdb searchWidget = SearchPageImdb.Navigate(Driver).GetSearchWidget();

            ImdbSearchModel searchModel = new ImdbSearchModel
            {
                Movie = searchName,
            };
            SearchPageImdb resultPage = searchWidget.SearchFor(searchModel);

            ResultWidgetImdb resultWidget = resultPage.GetResultWidget();

            var texts = resultWidget.GetResultLineContents();
            try
            {
                Assert.That(texts.Any(s => s.Contains(expectedText)));
            }
            catch (AssertionException ex)
            {
                TakeScreenshot();
                throw;
            }

        }
        private void TakeScreenshot()
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string screenshotName = baseDirectory + TestContext.CurrentContext.Test.Name + "_" +
                DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_error.png";
            screenshotName = screenshotName.Replace("\"","");
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile(screenshotName, ScreenshotImageFormat.Png);
            Process.Start(screenshotName);
        }
        static IEnumerable MovieData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\movies.xml");
            return
                from vars in doc.Descendants("movieData")
                let searchName = vars.Attribute("searchName").Value
                let expectedText = vars.Attribute("expectedText").Value
                select new object[] { searchName, expectedText };
        }
    }
}
