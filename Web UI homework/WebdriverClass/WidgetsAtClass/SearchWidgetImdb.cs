﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.Models;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class SearchWidgetImdb : BasePage
    {
        public SearchWidgetImdb(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement SearchInput => Driver.FindElement(By.Id("suggestion-search"));
        public IWebElement SearchButton => Driver.FindElement(By.Id("suggestion-search-button"));

        public void SetSearch(String movie)
        {
            SearchInput.SendKeys(movie);
        }

        public SearchPageImdb SearchFor(ImdbSearchModel search)
        {
            SetSearch(search.Movie);
            return ClickSearchButton();
        }

        public SearchPageImdb ClickSearchButton()
        {
            // Click on searchButton
            SearchButton.Click();
            // Return a SearchPage instance
            return new SearchPageImdb(Driver);
        }
    }
}
