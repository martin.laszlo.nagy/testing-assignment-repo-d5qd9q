﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    public class ResultWidgetImdb : BasePage
    {
        public ResultWidgetImdb(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement findList => Driver.FindElement(By.ClassName("findList"));

        private List<IWebElement> Lines => findList.FindElements(By.XPath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr")).ToList();

        public int GetNoOfResults()
        {
            return Lines.Count;
        }

        public List<string> GetResultLineContents()
        {
            return Lines.Select(s => s.Text).ToList();
        }
    }
}
