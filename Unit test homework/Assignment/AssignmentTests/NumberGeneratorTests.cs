﻿using Assignment.Numbers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTests
{
    [TestFixture]
    public class NumberGeneratorTests
    {
        NumberGenerator numberGenerator;
        [SetUp]
        public void Setup()
        {
            numberGenerator = new NumberGenerator();
        }

        [TestCase(4)]
        [TestCase(15)]
        [TestCase(77)]
        public void GenerateEven_Should_ReturnEvenNumber_When_GivenAValidLimit(int limit)
        {
            var result = numberGenerator.GenerateEven(limit);

            Assert.IsTrue(result % 2 == 0);
        }

        [TestCase(4)]
        [TestCase(15)]
        [TestCase(77)]
        public void GenerateEven_Should_ReturnEvenNumberLessOrEqualToLimit_When_GivenAValidLimit(int limit)
        {
            var result = numberGenerator.GenerateEven(limit);

            Assert.IsTrue(result <= limit);
        }

        [TestCase(-10)]
        public void GenerateEven_Should_ThrowArgumentOutOfRangeException_When_GivenANegativeNumber(int limit)
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberGenerator.GenerateEven(limit));
        }

        [TestCase(1)]
        [TestCase(15)]
        [TestCase(77)]
        public void GenerateOdd_Should_ReturnOddNumber_When_GivenAValidLimit(int limit)
        {
            var result = numberGenerator.GenerateOdd(limit);

            Assert.IsFalse(result % 2 == 0);
        }

        [TestCase(4)]
        [TestCase(15)]
        [TestCase(77)]
        public void GenerateOdd_Should_ReturnEvenNumberLessOrEqualToLimit_When_GivenAValidLimit(int limit)
        {
            var result = numberGenerator.GenerateOdd(limit);

            Assert.IsTrue(result <= limit);
        }

        [TestCase(-10)]
        [TestCase(0)]
        public void GenerateOdd_Should_ThrowArgumentOutOfRangeException_When_GivenANumberLessThanOne(int limit)
        {
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberGenerator.GenerateOdd(limit));
        }
    }
}
