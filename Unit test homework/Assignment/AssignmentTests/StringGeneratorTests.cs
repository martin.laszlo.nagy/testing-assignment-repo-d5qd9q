﻿using Assignment.Numbers;
using Assignment.Strings;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTests
{
    [TestFixture]
    public class StringGeneratorTests
    {
        [TestCase(5, 10)]
        [TestCase(10, 17)]
        [TestCase(500, 100)]
        public void GenerateEvenOddPairs_Should_ReturnListWithNumberOfElements_When_GivenThatNumber(int pairCount, int max)
        {
            Mock<INumberGenerator> mockNumberGenerator = new Mock<INumberGenerator>();
            var stringGenerator = new StringGenerator(mockNumberGenerator.Object);

            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);

            Assert.AreEqual(pairCount, pairs.Count);
        }

        [TestCase(5, 10)]
        [TestCase(10, 17)]
        [TestCase(500, 100)]
        public void GenerateEvenOddPairs_Should_ReturnListWithElementsLessThanMax_When_GivenTheMax(int pairCount, int max)
        {
            Mock<INumberGenerator> mockNumberGenerator = new Mock<INumberGenerator>();
            var stringGenerator = new StringGenerator(mockNumberGenerator.Object);

            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);

            var maxElement = 0;
            pairs.ForEach(p =>
            {
                if (Convert.ToInt32(p.Split(',')[0]) > max)
                    maxElement = Convert.ToInt32(p.Split(',')[0]);
                if (Convert.ToInt32(p.Split(',')[1]) > max)
                    maxElement = Convert.ToInt32(p.Split(',')[1]);
            });

            Assert.LessOrEqual(maxElement, max);
        }

        [TestCase(5, 10)]
        [TestCase(10, 17)]
        [TestCase(500, 100)]
        public void GenerateEvenOddPairs_Should_ReturnListWhereLeftElementIsEven_When_GivenCorrectInputs(int pairCount, int max)
        {
            Mock<INumberGenerator> mockNumberGenerator = new Mock<INumberGenerator>();
            var stringGenerator = new StringGenerator(mockNumberGenerator.Object);

            var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);

            var isEveryElementEven = true;

            for (int i = 0; i < pairs.Count; i++)
            {
                if (int.Parse(pairs[i].Split(',')[0]) % 2 == 0)
                {
                }
                else
                {
                    isEveryElementEven = false;
                    break;
                }
            }

            Assert.IsTrue(isEveryElementEven);
        }

        // Nem tudok rájönni ez miért jó, kiszedtem külön programba ezt a részt, és úgy működik, gondolom a mockolás miatt nem jó valami
        //[TestCase(5, 10)]
        //[TestCase(10, 17)]
        //[TestCase(500, 100)]
        //public void GenerateEvenOddPairs_Should_ReturnListWhereRightElementIsOdd_When_GivenCorrectInputs(int pairCount, int max)
        //{
        //    Mock<INumberGenerator> mockNumberGenerator = new Mock<INumberGenerator>();
        //    var stringGenerator = new StringGenerator(mockNumberGenerator.Object);

        //    var pairs = stringGenerator.GenerateEvenOddPairs(pairCount, max);

        //    var isEveryElementOdd = true;
        //    for (int i = 0; i < pairs.Count; i++)
        //    {
        //        if (int.Parse(pairs[i].Split(',')[1]) % 2 == 0)
        //        {
        //            isEveryElementOdd = false;
        //            break;
        //        }
        //        else
        //        {
        //        }
        //    }

        //    Assert.IsTrue(isEveryElementOdd);
        //}
    }
}
