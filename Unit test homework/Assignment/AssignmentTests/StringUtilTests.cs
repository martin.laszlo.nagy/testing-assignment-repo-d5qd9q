﻿using Assignment.Strings;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTests
{
    [TestFixture]
    public class StringUtilTests
    {
        StringUtil stringUtil;
        [SetUp]
        public void Setup()
        {
            stringUtil = new StringUtil();
        }

        [TestCase("apa")]
        [TestCase("keretkarakterek")]
        [TestCase("kévék")]
        public void IsPalindrom_Should_ReturnTrue_When_InputIsPalindromString(string str)
        {
            var result = stringUtil.IsPalindrom(str);

            Assert.IsTrue(result);
        }

        [TestCase("nem palindrom string")]
        [TestCase("1241411")]
        [TestCase("asd;asd")]
        public void IsPalindrom_Should_ReturnFalse_When_InputIsNotPalindromString(string str)
        {
            var result = stringUtil.IsPalindrom(str);

            Assert.IsFalse(result);
        }
    }
}
