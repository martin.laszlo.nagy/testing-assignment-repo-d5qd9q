﻿using Assignment.Numbers;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTests
{
    [TestFixture]
    public class NumberUtilsTests
    {
        NumberUtils numberUtils;

        [SetUp]
        public void Setup()
        {
            numberUtils = new NumberUtils();
        }

        [TestCase(5, 2)]
        [TestCase(10, 4)]
        [TestCase(3, 2)]
        public void GetDivisors_Should_ReturnListWithNumberOfElements_When_GivenThatNumber(int number, int itemCount)
        {
            var list = numberUtils.GetDivisors(number);

            Assert.AreEqual(itemCount, list.Count);
        }

        [TestCase]
        public void GetDivisors_Should_ReturnListWithRightDivisors_When_GivenAPositiveNumber()
        {
            var list = numberUtils.GetDivisors(15);

            List<int> expected = new List<int> { 1, 3, 5, 15 };
            Assert.AreEqual(expected, list.OrderBy(l => l));
        }

        [TestCase]
        public void GetDivisors_Should_ReturnListWithZeroDivisors_When_GivenZero()
        {
            var list = numberUtils.GetDivisors(0);

            List<int> expected = new List<int> { };
            Assert.AreEqual(expected, list);
        }

        [TestCase(2)]
        [TestCase(7583)]
        [TestCase(5021)]
        public void IsPrime_Should_ReturnTrue_When_GivenAPrimeNumber(int number)
        {
            var result = numberUtils.IsPrime(number);
            Assert.IsTrue(result);
        }

        [TestCase(12)]
        [TestCase(4444)]
        [TestCase(5000)]
        public void IsPrime_Should_ReturnFalse_When_GivenANotPrimeNumber(int number)
        {
            var result = numberUtils.IsPrime(number);
            Assert.IsFalse(result);
        }

        [TestCase(-12)]
        [TestCase(-4444)]
        [TestCase(-5000)]
        public void IsPrime_Should_ReturnFalse_When_GivenANegativeNumber(int number)
        {
            var result = numberUtils.IsPrime(number);
            Assert.IsFalse(result);
        }

        [TestCase(0)]
        [TestCase(-100)]
        [TestCase(22)]
        public void EvenOrOdd_Should_ReturnEven_When_GivenAnEvenNumber(int number)
        {
            var result = numberUtils.EvenOrOdd(number);

            Assert.AreEqual("even", result);
        }

        [TestCase(11)]
        [TestCase(-23)]
        [TestCase(23)]
        public void EvenOrOdd_Should_ReturnOdd_When_GivenAnOddNumber(int number)
        {
            var result = numberUtils.EvenOrOdd(number);

            Assert.AreEqual("odd", result);
        }
    }
}
